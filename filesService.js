
const fs = require('fs');
const folder='./files';
const path=require('path');

function createFile (req, res, next) {

  if(!req.body.filename){
    res.status(400).send({ "message": "Failure", "error": "No file name" });
  }else{
    let end=path.extname(req.body.filename);
    let formats=[ '.log', '.txt', '.json', '.yaml', '.xml', '.js'];
    let formatExist=formats.reduce((prev, cur)=>prev||cur==end, '')
    if(!formatExist){
      res.status(400).send({ "message": "Failure", "error": "Incorrect format" });
    }else{
      fs.writeFile(path.join(__dirname, 'files', req.body.filename), req.body.content, function (err) {
        if (err){
          res.status(400).send({ "message": "Failure", "error": err});
        }else{
            res.status(200).send({ "message": "File created successfully" });
        }
  
      })
    }
  }
  
}

function getFiles (req, res, next) {
  
  let arr=[];

  fs.readdir(folder, (err, files) => {
    if(err){
      res.status(400).send("Something bad is going to happen...", err);
    }else{    
      files.forEach(file => {
      arr.push(file);
    })
      let obj={'message': 'Success', 'files': arr}
      res.status(200).send(obj);
  }
  });
}

const getFile = (req, res, next) => 
{
  console.log(req.url)
  fs.readFile(path.join(__dirname, 'files', req.url), 'utf8', (err, data) => {
    if (err) {
      res.status(400).send({
        "message": "Not successful",
        "Error":err})
    }else{
      fs.stat(path.join(__dirname, 'files', req.url), (error, stats) => {
        if (error) {
          res.status(400).send({
            "message": "Not successful",
            "Error":error})
        }else{
          res.status(200).send({
            "message": "Success",
            "filename": req.url.replace('/', ''),
            "content": data,
            "extension": path.extname(req.url).replace('.', ''),
            "uploadedDate": stats.birthtime});            
        }    
      });
    }
  });  
}

// Other functions - editFile, deleteFile

// path.extName('file.txt') ---> '.txt'
// fs.writeFile ({ flag: 'a' }) ---> adds content to the file

module.exports = {
  createFile,
  getFiles,
  getFile
}
